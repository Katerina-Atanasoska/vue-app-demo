import { createRouter, createWebHistory } from "vue-router";

const Home = () => import('@/components/pages/home/Home.vue');
const About = () => import('@/components/pages/about/About.vue');

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
    },
    {
        path: '/about-us',
        name: 'about',
        component: About,
    },
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router